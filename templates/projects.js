function filterProjects(id) {
    id = id.replace(/:[\w-]+/, "");
    if (id === "") {
        showAllProjects()
    } else {
        document.getElementById("show-all").hidden = false;

        if (document.getElementById(id)) {
            document.querySelector(`.project-card:has(#${id})`).hidden = false;
            document.querySelectorAll(`.project-card:not(:has(#${id}))`).forEach((el) => el.hidden = true);
        } else {
            document.querySelectorAll(`.project-card:has(a[href="#${id}"])`).forEach((el) => el.hidden = false);
            document.querySelectorAll(`.project-card:not(:has(a[href="#${id}"]))`).forEach((el) => el.hidden = true);
        }
    }
}
function showAllProjects() {
    document.getElementById("show-all").hidden = true;
    document.querySelectorAll(".project-card").forEach((el) => el.hidden = false);
}

window.addEventListener("hashchange", (e) => {
    let id = new URL(e.newURL).hash.substring(1);
    filterProjects(id);
});
window.addEventListener("DOMContentLoaded", () => {
    let id = location.hash.substring(1);
    filterProjects(id);
});

let id = location.hash.substring(1);
filterProjects(id);